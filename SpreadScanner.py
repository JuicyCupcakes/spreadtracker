import logging
import threading
import Queue
import time
import gdax
import krakenex
import json
import Tkinter
import tkFont
import ttk
import winsound


class SpreadTracker:
    def __init__(self):
        self.krakenC = krakenex.API()
        self.gdaxC = gdax.PublicClient()
        self.ETHinfo = {'ask': 0, 'bid': 0, 'spread': 0}
        self.LTCinfo = {'ask': 0, 'bid': 0, 'spread': 0}

    def getAskBidSpreadETHLTC(self):
        #Get Eth pricing on kraken
        try:
            krakenETHLTC = self.krakenC.query_public('Ticker', {'pair': 'ETHEUR,LTCEUR'})
            krakenETHask = float(krakenETHLTC['result']['XETHZEUR']['a'][0])
            krakenLTCask = float(krakenETHLTC['result']['XLTCZEUR']['a'][0])


            gdaxETH = self.gdaxC.get_product_ticker(product_id='ETH-EUR') #could use websocket, however, kraken doesn't support it
            gdaxLTC = self.gdaxC.get_product_ticker(product_id='LTC-EUR')
            gdaxETHbid = float(gdaxETH['bid'])
            gdaxLTCbid = float(gdaxLTC['bid'])



            spreadETH = gdaxETHbid - krakenETHask
            spreadLTC = gdaxLTCbid - krakenLTCask

            self.ETHinfo = {'ask': krakenETHask, 'bid': gdaxETHbid, 'spread': spreadETH}
            self.LTCinfo = {'ask': krakenLTCask, 'bid': gdaxLTCbid, 'spread': spreadLTC}
        except:
            print "Error getting data, kraken probably failed"


class listview(Tkinter.Frame):
    '''
    classdocs
    '''
    def __init__(self, parent):
        self.exposure = 1000
        '''
        Constructor
        '''
        Tkinter.Frame.__init__(self, parent)
        self.parent=parent
        self.initialize_user_interface()
        self.UpdateData()

    def UpdateExposure(self):
        self.exposure = int(self.E1.get())
        print self.exposure


    def initialize_user_interface(self):
        """Draw a user interface allowing the user to type
        items and insert them into the treeview
        """
        self.parent.title("Tracker data")
        self.parent.grid_rowconfigure(0,weight=1)
        self.parent.grid_columnconfigure(0,weight=1)
        self.parent.config(background="lavender")


        # Define the different GUI widgets
        # self.ETHlabel = Tkinter.Label(self.parent, text = "ETH Moving Average (30 min max):")
        # self.ETHlabel.grid(row=0, column=1)
        # self.LTClabel = Tkinter.Label(self.parent, text="LTC Moving Average (30 min max):")
        # self.LTClabel.grid(row=1, column=1)
        self.L1 = Tkinter.Label(self.parent, text="Exposure per trade (EUR)")
        self.L1.grid(row=0, column=1)
        self.E1 = Tkinter.Entry(self.parent, bd=5)
        self.E1.insert(Tkinter.END, str(self.exposure))
        self.E1.grid(row=0, column=2)
        self.exit_button = Tkinter.Button(self.parent, text = "Enter", command = self.UpdateExposure)
        self.exit_button.grid(row = 0, column = 3)

        # Set the treeview
        self.tree = ttk.Treeview( self.parent, columns=('Kraken ETH Ask', 'GDAX ETH bid', '3','4','5','6'))
        self.tree.heading('#0', text='Count')
        self.tree.heading('#1', text='Kraken ETH Ask')
        self.tree.heading('#2', text='GDAX ETH bid')
        self.tree.heading('#3', text='Profit (%)')
        self.tree.heading('#4', text='Kraken LTC Ask')
        self.tree.heading('#5', text='GDAX LTC bid')
        self.tree.heading('#6', text='ROI (Profit %)')
        self.tree.column('#0', stretch=Tkinter.YES)
        self.tree.column('#1', stretch=Tkinter.YES)
        self.tree.column('#2', stretch=Tkinter.YES)
        self.tree.column('#3', stretch=Tkinter.YES)
        self.tree.column('#4', stretch=Tkinter.YES)
        self.tree.column('#5', stretch=Tkinter.YES)
        self.tree.column('#6', stretch=Tkinter.YES)
        self.tree.grid(row=4, columnspan=4, sticky='nsew')
        self.treeview = self.tree
        # Initialize the counter
        self.i = 0


    def UpdateData(self):
        try:
            exchangedata = q.get(block=False)
        except Queue.Empty:
            exchangedata = [{'ask': 0, 'bid': 0, 'spread': 0}, {'ask': 0, 'bid': 0, 'spread': 0}]
        ETHinfo = exchangedata[0]
        LTCinfo = exchangedata[1]
        if ETHinfo['ask'] != 0:
            krakenWFeeETH = 0.005 #absolute ETH
            krakenWFeeLTC = 0.001 #absolute
            krakenMakerFee = 0.0026 #percentage/100

            gdaxMakerFee = 0.003 #percentage/100
            gdaxSEPAwithdrawalFee = 0.15 #absolute eur

            buypower = self.exposure - (krakenMakerFee * self.exposure)

            #eth
            ETHkrakenOut = buypower / ETHinfo['ask'] - krakenWFeeETH
            gdaxEURbidETH = ETHkrakenOut*ETHinfo['bid']
            bankEURETH = gdaxEURbidETH - gdaxEURbidETH*gdaxMakerFee - gdaxSEPAwithdrawalFee

            #ltc
            LTCkrakenOut = buypower / LTCinfo['ask'] - krakenWFeeLTC
            gdaxEURbidLTC = LTCkrakenOut * LTCinfo['bid']
            bankEURLTC = gdaxEURbidLTC - gdaxEURbidLTC * gdaxMakerFee - gdaxSEPAwithdrawalFee

            profitETH = round(((bankEURETH/self.exposure)*100-100),2)
            profitLTC = round(((bankEURLTC/self.exposure) * 100 - 100), 2)

            if profitETH > 2 or profitLTC > 2:
                winsound.PlaySound('R2D2-yeah.wav', winsound.SND_FILENAME)

            spreadtextETH = str(bankEURETH) + " (" + str(profitETH) + ")"
            spreadtextLTC = str(bankEURLTC) + " (" + str(profitLTC) + ")"

            self.treeview.insert('', 'end', text=str(self.i),
                                 values=(ETHinfo['ask'], ETHinfo['bid'], spreadtextETH, LTCinfo['ask'], LTCinfo['bid'], spreadtextLTC))

            self.treeview.yview_moveto(1)
            self.i += 1

        self.parent.after(5000, self.UpdateData)


q = Queue.Queue()

def runGUI():
    root = Tkinter.Tk()
    d = listview(root)
    root.mainloop()


def updateDataThread():
    ST = SpreadTracker()
    print 'Thread started!'

    while True:
        ST.getAskBidSpreadETHLTC()
        exchangedata = [ST.ETHinfo, ST.LTCinfo]
        print exchangedata
        q.put(exchangedata)
        time.sleep(5)


def main():
    print "Main!"
    t = threading.Thread(target=updateDataThread)
    t.daemon = True
    t.start()
    runGUI()


if __name__=="__main__":
    main()

